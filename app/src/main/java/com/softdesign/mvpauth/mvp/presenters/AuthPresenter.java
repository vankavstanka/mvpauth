package com.softdesign.mvpauth.mvp.presenters;

import android.support.annotation.Nullable;

import com.softdesign.mvpauth.mvp.models.AuthModel;
import com.softdesign.mvpauth.mvp.views.IAuthView;
import com.softdesign.mvpauth.ui.custom_viewers.AuthPanel;

/**
 * Created by Дима on 22.10.2016.
 */

public class AuthPresenter implements IAuthPresenter {
    private static AuthPresenter ourInstance = new AuthPresenter();
    private AuthModel mAuthModel;
    private IAuthView mAuthView;

    private AuthPresenter() {
        mAuthModel = new AuthModel();
    }

    public static AuthPresenter getInstance() {
        return ourInstance;
    }

    @Override
    public void takeView(IAuthView authView) {
        mAuthView = authView;
    }

    @Override
    public void dropView() {
        mAuthView = null;
    }

    @Override
    public void initView() {
        if (getView() != null) {
            if (checkUserAuth()) {
                getView().hideLoginBtn();
            } else
                getView().showLoginBtn();
        }
    }

    @Nullable
    @Override
    public IAuthView getView() {
        return mAuthView;
    }

    @Override
    public void clickOnLogin() {
        if (getView() != null && getView().getAuthPanel() != null) {
            if (getView().getAuthPanel().isIdle()) {
                getView().getAuthPanel().setCustomState(AuthPanel.LOGIN_STATE);
            } else
                //TODO 17.10.2016 auth user
                mAuthModel.loginUser(getView().getAuthPanel().getUserEmail(), getView().getAuthPanel().getUserPassword());
            getView().showMassage("request for user auth");
        }
     }

    public void clickOnFb () {
        if (getView() != null) {
            getView().showMassage("clickOnFb");
        }
    }

    public void clickOnVk () {
        if (getView() != null) {
            getView().showMassage("clickOnVk");
        }
    }

    public void clickOnTwitter () {
        if (getView() != null) {
            getView().showMassage("clickOnTwitter");
        }
    }

    public void clickOnShowCatalog () {
        if (getView() != null) {
            getView().showMassage("Показать каталог");
        }
    }

    public boolean checkUserAuth () {
        return mAuthModel.isAuthUser();
    }
}

